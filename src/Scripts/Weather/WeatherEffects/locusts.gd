extends Node2D

enum TYPE {INVALID, ACID, BLIZZARD, FIRE, RAIN, METEORITE, LIGHTNING, LOCUSTS, TORNADO}
const type = TYPE.LOCUSTS
const deadly = true

var speed: float
var spawn: Vector2
var target: Vector2

var _t: float = 0

func setup(Spawn: Vector2, Target: Node2D, Speed: float) -> void:
	spawn = Spawn
	global_position = Spawn
	target = Spawn + Spawn.direction_to(Target.global_position)
	speed = Speed
	if target.x < 0:
		scale.x = -1

func _process(delta) -> void:
	_t += delta * speed
	global_position = spawn.linear_interpolate(target, _t)


func delete() -> void:
	# TODO : Stop sound or some shit
	queue_free()
