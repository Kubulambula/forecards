tool
extends Node2D

# TODO : Když je město zničený přidat particle efekty kouře?

const _CITY_VISUAL_RESOURCE_DIR = "res://Assets/Art/Cities/"

export(int, 0, 4) var city: int = 0 setget set_city
export(int, 0, 1) var condition: int = 0 setget set_condition
export(bool) var use_override: bool = false setget set_override
export(int, 1, 1000) var override_radius: int = 1 setget set_radius

onready var _city: Sprite = $City
onready var _border: Sprite = $Border
onready var _card_container: Node2D = $CardContainer
onready var _collision_polygon: CollisionPolygon2D = $CardBox/CollisionPolygonShape2D

# 0 - OK, 1 - DESTOROYED
var _city_conditions: Array = []

var active_card: Node2D = null

const _BORDER_COLORS: Dictionary = {
		"OFF": Color(1, 1, 1, 0), # No border
		"SELECT": Color(0, 1, 0, 1), # Card will be placed on city
		"REPLACE": Color(0, 0, 1, 1), # Card will be placed on city replacing card currently on city
		"INVALID": Color(1, 0, 0, 1), # City is destroyed and card cannon be placed
}

const _CITY_SHAPE_RADIUS: Array = [
		150,
		150,
		150,
		150,
		210,
]

var ded = false


func set_city(value: int) -> void:
	city = value
	if get_node_or_null("City") and get_node_or_null("Border") and get_node_or_null("CardBox/CollisionPolygonShape2D"):
		_set_city()


func _set_city() -> void: # Because of the tool script the child nodes might not be ready when set_city() is called
	_city_conditions = [
			load(_CITY_VISUAL_RESOURCE_DIR + "city_" + str(city) + ".png"), # OK
			load(_CITY_VISUAL_RESOURCE_DIR + "city_" + str(city) + "_middle.png"), # Destroyed
			load(_CITY_VISUAL_RESOURCE_DIR + "city_" + str(city) + "_destroyed.png"), # Destroyed
	]
	$City.texture = _city_conditions[condition] # Clapm the condition
	$Border.texture = load(_CITY_VISUAL_RESOURCE_DIR + "city_" + str(city) + "_border.png")
	$CardBox/CollisionPolygonShape2D.shape = preload("res://addons/CollisionPolygonShape/CollisionPolygonShape2D/PolygonShape2D/CirclePolygonShape2D.gd").new()
	$CardBox/CollisionPolygonShape2D.shape.radius = _CITY_SHAPE_RADIUS[city]


func set_condition(value: int) -> void:
	condition = min(value, _city_conditions.size()-1) as int
	if condition > 0 and not ded:
		if condition == 1:
			$Sad.emitting = true
			$Smoke.set_emitting(true)
		elif condition == 2:
			$Dead.emitting = true
			$Smoke2.set_emitting(true)
			ded = true
	if condition == _city_conditions.size() - 1:
		GameState.active_cities.erase(self)
		GameDirector.check_cities()
	_set_city()
	if not Engine.editor_hint and condition < _city_conditions.size()-1:
		$DeathSound.play()


func set_override(value: bool) -> void:
	use_override = value
	if use_override:
		$CardBox/CollisionPolygonShape2D.shape.radius = override_radius
	else:
		$CardBox/CollisionPolygonShape2D.shape.radius = _CITY_SHAPE_RADIUS[city]


func set_radius(value: int) -> void:
	override_radius = value
	if use_override == true:
		$CardBox/CollisionPolygonShape2D.shape.radius = override_radius


func _ready() -> void:
	_border.self_modulate = _BORDER_COLORS["OFF"]
	add_to_group("cities")
	_set_city()
	if use_override == true:
		_collision_polygon.shape.radius = override_radius
	
	if not Engine.editor_hint:
		GameState.all_cities.append(self)
		if condition < _city_conditions.size()-1:
			GameState.active_cities.append(self)


func _exit_tree() -> void:
	if not Engine.editor_hint:
		GameState.all_cities.erase(self)
		GameState.active_cities.erase(self)


func take_card(card: Node2D) -> void:
	if condition < _city_conditions.size()-1: # City not yet destoryed
		if active_card and active_card != card: # City already has card
			active_card.transfer_card_to_canvas()
		var former_global = card.get_global_transform()
		card.parent.remove_child(card)
		_card_container.add_child(card)
		card.parent = _card_container
		card.responsible_parent = self
		card.global_transform = former_global
		card.state = card.STATE.CITY
		active_card = card
		card._mouse_inside = false
		card.z_index = 0
	else:
		GameState.card_in_hand.transfer_card_to_canvas()
		GameState.card_in_hand = null


func free_card(card: Node2D):
	_card_container.remove_child(card)
	card.z_index = 10
	card.parent = null
	card.responsible_parent = null
	active_card = null


func remove_card() -> void:
	active_card.state = active_card.STATE.DECK
	active_card = null


func has_card() -> bool:
	return true if active_card else false


# on = false - Unhighlight city of another city grabbed the highlight focus
func highlight(on: bool = true) -> void:
	if on:
		get_tree().call_group_flags(SceneTree.GROUP_CALL_REALTIME, "cities", "highlight", false)
		if condition < _city_conditions.size()-1: # City not destroyed
			if active_card: # City already has card - replace it
				_border.self_modulate = _BORDER_COLORS["REPLACE"]
			else: # City has free card container
				_border.self_modulate = _BORDER_COLORS["SELECT"]
		else: # City destroyed
			_border.self_modulate = _BORDER_COLORS["INVALID"]
	else:
		_border.self_modulate = _BORDER_COLORS["OFF"]


func get_card_container_global_transform() -> Transform2D:
	return _card_container.get_global_transform()


func get_global_polygon() -> PoolVector2Array:
	var polygon: PoolVector2Array = _collision_polygon.polygon
	for i in range(polygon.size()):
		polygon[i] = polygon[i] * global_scale + global_position
	return polygon


func _on_CardBox_area_entered(area):
	if area.is_in_group("weather"):
		if active_card:
			if active_card.type == area.get_parent().type:
				# City is protected
				active_card.call_deferred("transfer_card_to_canvas")
				$Happy.emitting = true
			else:
				# City is not protected (bad card)
				if area.get_parent().deadly or condition != _city_conditions.size() - 2:
					set_condition(condition + 1)
				if condition == _city_conditions.size() - 1:
					active_card.call_deferred("transfer_card_to_canvas")
		else:
			# City is not protected (no card)
			if area.get_parent().deadly or condition != _city_conditions.size() - 2:
					set_condition(condition + 1)
