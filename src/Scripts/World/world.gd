extends Node2D


# TODO : City particles, meteorite, city protected feedback, time survived



func _ready() -> void:
	GameState.set_active_scene(self)


func take_card(card: Node2D) -> void:
	add_child(card)
	card.parent = self
	card.responsible_parent = self


func free_card(card: Node2D) -> void:
	remove_child(card)
	card.parent = null
	card.responsible_parent = null


func get_camera() -> Camera2D:
	return $Centered/Camera2D as Camera2D
