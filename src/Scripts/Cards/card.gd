# Copyright (c) 2021 Jakub Janšta
# See License.md

extends Node2D

# BUG : když je karta puštěna nad místem, kde se po vysunutí balíčku objeví karta, tak ta karta nechytí mouse focus


enum STATE {DECK, HAND, CITY}
enum TYPE {INVALID, ACID, BLIZZARD, FIRE, RAIN, METEORITE, LIGHTNING, LOCUSTS, TORNADO}

const _card_type_visual_resource: Array = [
	"default",
	"acid",
	"blizzard",
	"fire",
	"rain",
	"meteorite",
	"lightning",
	"locusts",
	"tornado",
]

const SOUNDS = [
	preload("res://Assets/Sound/SFX/card1.wav"),
	preload("res://Assets/Sound/SFX/card2.wav"),
	preload("res://Assets/Sound/SFX/card3.wav"),
	preload("res://Assets/Sound/SFX/card4.wav"),
	preload("res://Assets/Sound/SFX/card5.wav"),
]


var type: int = 0 setget set_type
var state: int = 0 setget set_state

var _feasible_cities: Array = []

var parent: Node = null
var responsible_parent: Node = null

var _mouse_inside: bool = false
var _mouse_offset: Vector2 = Vector2(0,0)

var inworld_scale: Vector2 = Vector2(1.5, 1.5)
var default_scale: Vector2 = Vector2(1, 1)
var focus_scale: Vector2 = Vector2(1.5, 1.5)

var _mouse_box: Control = null
var _gfx: AnimatedSprite = null
var _collision_polygon: CollisionPolygon2D = null
var _sfx: AudioStreamPlayer = null


func set_type(card_type:int=0) -> void:
	if card_type < TYPE.size():
		type = card_type
		if _gfx:
			_gfx.animation = _card_type_visual_resource[card_type] + "_idle"
	else:
		push_error("Invalid card type. See TYPE")


func set_state(card_state:int=0) -> void:
	if card_state < STATE.size():
		state = card_state
	else:
		push_error("Invalid state. See STATE")


func _ready() -> void:
	_sfx = $Sfx
	_mouse_box = $MouseBox
	_gfx = $MouseBox/AnimatedSprite
	_collision_polygon = $MouseBox/CityBox/CollisionPolygon2D
	set_type(type)
	parent = get_parent()
	responsible_parent = parent


func _process(_delta: float) -> void:
	match state: # State machine fun stuff
		STATE.DECK: # In player hand ready to be taken
			lerp_global_transform_to(parent.global_transform)
			lerp_scale_aroud_pivot(focus_scale if _mouse_inside and not GameState.card_in_hand else default_scale)
		STATE.HAND: # Moving with hand
			var hand_transform: Transform2D = Transform2D()
			hand_transform = hand_transform.scaled(inworld_scale)
			hand_transform.origin = get_global_mouse_position() - (_mouse_offset * transform.x.x)
			lerp_global_transform_to(hand_transform, 0.05)
			lerp_scale_aroud_pivot(default_scale)
			
			if _feasible_cities:
				if _feasible_cities.size() > 1: # At least two cities are overlaping
					sort_feasible_cities_by_overlap_area()
				_feasible_cities[0].highlight()
			
		STATE.CITY: # Statonary on city
			lerp_global_transform_to(parent.global_transform)
			lerp_scale_aroud_pivot(focus_scale if _mouse_inside else default_scale)


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and _mouse_inside:
		match state:
			STATE.DECK:
				if event.pressed and not GameState.card_in_hand:
					_mouse_offset = _mouse_box.get_local_mouse_position()
					transfer_card_to_world()
					state = STATE.HAND
					GameState.card_in_hand = self
					play_random_sound()
					
			STATE.CITY:
				if event.pressed and not GameState.card_in_hand:
					_mouse_offset = _mouse_box.get_local_mouse_position()
					var target_global_transform = get_global_transform()
					responsible_parent.free_card(self)
					GameState.active_scene.take_card(self)
					global_transform = target_global_transform
					state = STATE.HAND
					GameState.card_in_hand = self
					play_random_sound()
					
			STATE.HAND:
				if not event.pressed:
					if _feasible_cities.size():
						var city = _feasible_cities[0]
						city.take_card(self)
						_gfx.animation = _card_type_visual_resource[type] + "_idle"
					else: # Card not over city
						transfer_card_to_canvas()
					GameState.card_in_hand = null
					play_random_sound()
	
	elif event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and _mouse_inside:
		if event.pressed:
			if state != STATE.DECK:
				transfer_card_to_canvas()
				GameState.card_in_hand = null
				play_random_sound()


func transfer_card_to_world() -> void:
	var target_canvas_transform = get_global_transform_with_canvas()
	GameState.active_deck.free_card(self) # GameState.active_deck is the responsible_parent
	GameState.active_scene.take_card(self)
	play_random_sound()
	# Calculate global new global transform that the global_transform_with_canvas stays the same with parent change to world
	global_transform.x = global_transform.x * target_canvas_transform.x * get_global_transform_with_canvas().affine_inverse().x
	global_transform.y = global_transform.y * target_canvas_transform.y * get_global_transform_with_canvas().affine_inverse().y
	global_transform.origin = (target_canvas_transform.origin - get_global_transform_with_canvas().origin) * GameState.get_active_scene_camera_zoom()


func transfer_card_to_canvas() -> void:
	var target_canvas_transform = get_global_transform_with_canvas()
	responsible_parent.free_card(self)
	GameState.active_deck.take_card(self)
	play_random_sound()
	# Calculate global new global transform that the global_transform_with_canvas stays the same with parent change to canvas
	global_transform.x = global_transform.x * target_canvas_transform.x * get_global_transform_with_canvas().affine_inverse().x
	global_transform.y = global_transform.y * target_canvas_transform.y * get_global_transform_with_canvas().affine_inverse().y
	global_transform.origin = target_canvas_transform.origin
	
	_mouse_inside = false
	_gfx.animation = _card_type_visual_resource[type] + "_idle"
	state = STATE.DECK

func lerp_global_transform_to(to: Transform2D, weight: float = 0.08) -> void:
	global_transform.origin = lerp(global_transform.origin, to.origin, (1 / weight) * get_process_delta_time())
	global_transform.x = lerp(global_transform.x, to.x, (1 / weight) * get_process_delta_time())
	global_transform.y = lerp(global_transform.y, to.y, (1 / weight) * get_process_delta_time())


func lerp_global_transform_without_origin_to(to: Transform2D, weight: float = 0.08) -> void:
	global_transform.x = lerp(global_transform.x, to.x, (1 / weight) * get_process_delta_time())
	global_transform.y = lerp(global_transform.y, to.y, (1 / weight) * get_process_delta_time())


# Lerps MouseBoxes rect_scale aroun pivot that is in bottom center
func lerp_scale_aroud_pivot(to: Vector2, weight: float = 0.08) -> void:
	_mouse_box.rect_scale = lerp(_mouse_box.rect_scale, to, (1 / weight) * get_process_delta_time())


func _on_MouseBox_mouse_entered() -> void:
	if state == STATE.DECK and global_position.distance_to(parent.global_position) > 100: # TODO : This is a random value so tweak this (this is used when player unholds the card near its origin so that when transfered to deck, the mouse is still inside)
		_mouse_inside = false
		_gfx.animation = _card_type_visual_resource[type] + "_idle"
	else:
		_mouse_inside = true
		play_random_sound()
		_gfx.animation = _card_type_visual_resource[type]


func _on_MouseBox_mouse_exited() -> void:
	_mouse_inside = false
	play_random_sound()
	_gfx.animation = _card_type_visual_resource[type] + "_idle"


func _on_CityBox_area_entered(area: Area2D) -> void:
	_feasible_cities.append(area.get_parent())


func _on_CityBox_area_exited(area: Area2D) -> void:
	_feasible_cities.erase(area.get_parent())
	if not _feasible_cities: # No cities under card
		get_tree().call_group_flags(SceneTree.GROUP_CALL_REALTIME, "cities", "highlight", false)


func get_global_polygon() -> PoolVector2Array:
	var polygon: PoolVector2Array = _collision_polygon.polygon
	for i in range(polygon.size()):
		polygon[i] = polygon[i] * global_scale + global_position
	return polygon


func get_intersection_area_with_city(city: Node2D) -> float:
	var intersection: Array = Geometry.intersect_polygons_2d(
			get_global_polygon(), 
			city.get_global_polygon()
			)
	return Util.get_polygon_area(PoolVector2Array(intersection[0])) if intersection else 0.0


func sort_feasible_cities_by_overlap_area() -> void:
	_feasible_cities.sort_custom(Util, "sort_cities_by_area_custom_sort_helper")


func play_random_sound() -> void:
	_sfx.stream = SOUNDS[randi() % SOUNDS.size()]
	_sfx.play()
