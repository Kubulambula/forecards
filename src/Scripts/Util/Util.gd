class_name Util
extends Object


# Sholelace formula
static func get_polygon_area(polygon: PoolVector2Array) -> float:
	var area: float = 0
	# PoolVector2Array is passed by value and not reference so modifying is ok
	polygon.append(polygon[0])
	for i in range(polygon.size()-1):
		area += polygon[i].x * polygon[i+1].y
		area -= polygon[i].y * polygon[i+1].x
	return abs(area / 2)


static func sort_cities_by_area_custom_sort_helper(a: Node2D, b: Node2D) -> bool:
	return true if GameState.card_in_hand.get_intersection_area_with_city(a) > GameState.card_in_hand.get_intersection_area_with_city(b) else false


static func remap_value_between_ranges(value: float, old_range: Vector2, new_range: Vector2) -> float:
	return ((value - old_range.x) / (old_range.y - old_range.x)) * (new_range.y - new_range.x) + new_range.x
