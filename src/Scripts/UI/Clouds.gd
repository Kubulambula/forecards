extends Node2D


func _ready():
	var clouds = get_tree().get_nodes_in_group("clouds")
	for cloud in clouds:
		cloud.seek(20, false)
		cloud.play()
	for mrak in get_tree().get_nodes_in_group("mrak"):
		mrak.z_index = 5


func _on_AnimationPlayer_animation_finished(_cloud):
	$AnimationPlayer.play_backwards("cloud")


func _on_AnimationPlayer5_animation_finished(_cloud):
	$AnimationPlayer5.play_backwards("cloud")


func _on_AnimationPlayer2_animation_finished(_cloud):
	$AnimationPlayer2.play_backwards("cloud")


func _on_AnimationPlayer3_animation_finished(_cloud):
	$AnimationPlayer3.play_backwards("cloud")


func _on_AnimationPlayer4_animation_finished(_cloud):
	$AnimationPlayer3.play_backwards("cloud")


func _on_AnimationPlayer6_animation_finished(_cloud):
	$AnimationPlayer6.play_backwards("cloud")
