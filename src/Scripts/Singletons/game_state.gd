extends Node

var active_scene: Node = null setget set_active_scene
var _active_scene_camera: Camera2D = null

var active_deck: CanvasLayer = null

var card_in_hand: Node2D = null

var all_cities: Array = []
var active_cities: Array = [] # Cities that have not yet been destroyed 

var spawner: Node2D = null


func set_active_scene(scene: Node) -> void:
	active_scene = scene
	if scene.has_method("get_camera"):
		_active_scene_camera = scene.call("get_camera") as Camera2D
	else:
		var camera = scene.get_node_or_null("Camera2D") as Camera2D
		if camera:
			_active_scene_camera = camera
		
	if not _active_scene_camera:
		push_warning("No Camera2D found in the active scene. Add \"get_camera() -> Camera2D\" method to the scene root script to resolve the error.")


func get_active_scene_camera_zoom() -> Vector2:
	return _active_scene_camera.zoom if _active_scene_camera else Vector2(1,1)
