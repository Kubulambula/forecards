extends Node

var time_survided = 0 # In seconds


func _physics_process(delta):
	time_survided += delta


func get_time_survived() -> Array:
	time_survided = int(round(time_survided))
	var m = int(time_survided / 60)
	var s = time_survided - m * 60
	return [m, s]
