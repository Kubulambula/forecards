extends Node


var _music_stream: AudioStreamPlayer = AudioStreamPlayer.new()
var _tw: Tween = Tween.new()

var mute_sfx = false


func _ready():
	set_pause_mode(PAUSE_MODE_PROCESS)
	_music_stream.stream = preload("res://Assets/Sound/Music/bojova_hudba.wav")
	_music_stream.set_bus("Music")
	add_child(_music_stream)
	add_child(_tw)
	_music_stream.play()
#	filter_on(3)


func filter_on(duration: float = 1):
	# warning-ignore:return_value_discarded
	_tw.interpolate_method(self, "_apply", 0, 1, duration, Tween.TRANS_QUAD, Tween.EASE_OUT, 0.25)
	# warning-ignore:return_value_discarded
	_tw.start()


func filter_off(duration: float = 1):
	# warning-ignore:return_value_discarded
	_tw.interpolate_method(self, "_apply", 1, 0, duration, Tween.TRANS_QUAD, Tween.EASE_OUT)
	# warning-ignore:return_value_discarded
	_tw.start()


func _apply(progress):
	var effect: AudioEffectEQ = AudioServer.get_bus_effect(AudioServer.get_bus_index("Music"), 0)
	effect.set_band_gain_db(0, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -5)))
	effect.set_band_gain_db(1, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -5)))
	effect.set_band_gain_db(2, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -5)))
	effect.set_band_gain_db(3, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -8)))
	effect.set_band_gain_db(4, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -13)))
	effect.set_band_gain_db(5, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -11)))
	effect.set_band_gain_db(6, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -10)))
	effect.set_band_gain_db(7, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -8)))
	effect.set_band_gain_db(8, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -7)))
	effect.set_band_gain_db(9, Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -5)))
	if mute_sfx:
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), Util.remap_value_between_ranges(progress, Vector2(0,1), Vector2(0, -80)))
