extends Node


var cm: CanvasModulate = CanvasModulate.new()
var tw: Tween = Tween.new()
var restarting = false

var intro_shown: bool = false


func _ready():
	set_pause_mode(PAUSE_MODE_PROCESS)
	add_child(cm)
	add_child(tw)


func blink():
	# warning-ignore:return_value_discarded
	tw.interpolate_property(cm, "color", Color.white, Color.black, 0.15, Tween.TRANS_QUAD, Tween.EASE_IN)
	# warning-ignore:return_value_discarded
	tw.start()
	yield(get_tree().create_timer(0.15), "timeout")
	# warning-ignore:return_value_discarded
	tw.interpolate_property(cm, "color", Color.black, Color.white, 0.15, Tween.TRANS_QUAD, Tween.EASE_IN)
	# warning-ignore:return_value_discarded
	tw.start()


func restart() -> void:
	intro_shown = true
	
	restarting = true
	# warning-ignore:return_value_discarded
	tw.interpolate_property(cm, "color", Color.white, Color.black, 0.25, Tween.TRANS_QUAD, Tween.EASE_IN)
	# warning-ignore:return_value_discarded
	tw.start()
	
	GameState.card_in_hand = null
	GameDirector.running = true
	GameDirector.passed_effects = 0
#	GameDirector.set_difficulty(0)
#	Music.filter_on(4)
	yield(get_tree().create_timer(0.25), "timeout")
	# warning-ignore:return_value_discarded
	get_tree().reload_current_scene()
	
	# warning-ignore:return_value_discarded
	tw.interpolate_property(cm, "color", Color.black, Color.white, 0.25, Tween.TRANS_QUAD, Tween.EASE_IN)
	# warning-ignore:return_value_discarded
	tw.start()
