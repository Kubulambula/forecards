extends CanvasLayer


onready var _containers: Node2D = $Containers
onready var _temp_container: Node2D = $"Containers/0/TempCardContainer"
onready var _viewport = get_viewport()
onready var _hidden_mouse_box_rect: Rect2 = $MouseBoxCardsHidden.get_rect()
onready var _visible_mouse_box_rect: Rect2 = $MouseBoxCardsVisible.get_rect()

var _all_cards: Array = [] # TODO : free all cards after day end (don't forget to reset cities states) and add them back to deck and add new ones
var deck_cards: Array = []

var deck_target_offset = 0

# Offset balíčku po ose Y. Mezi nimi se lerpuje
var deck_offset_visible = 0
var deck_offset_hidden = 180
# Lerp weight balíčku po ose Y
var deck_lerp_weight = 0.1

# TODO : when hovering over a card the other cards shoul move aside (use signals or some shit)


func _ready() -> void:
	GameState.active_deck = self


func add_card(card: Node2D) -> void:
	if not card in _all_cards and _all_cards.size() < _containers.get_child_count()-1:
		_all_cards.append(card)
		_temp_container.add_child(card)
#		card.global_position = _temp_container.global_position
		take_card(card)


# This is unsafe, but because cards can be added only trought deck it is fine
func take_card(card: Node2D) -> void:
	deck_cards.append(card)
	if not card.parent:
		var new_container = _containers.get_child(deck_cards.size()).get_child(deck_cards.size()-1)
		new_container.add_child(card)
		card.parent = new_container
	update_card_containers()
	card.responsible_parent = self
	card.state = card.STATE.DECK
	$CardsUp.play()


func free_card(card: Node2D) -> void:
	deck_cards.erase(card)
	card.parent.remove_child(card)
	update_card_containers()
	card.parent = null
	card.responsible_parent = null
	card.state = card.STATE.HAND


func update_card_containers() -> void:
	var new_container_group = _containers.get_child(deck_cards.size())
	var first_free_container_index = 0
	for card in _all_cards:
		if card in deck_cards:
			var new_parent = new_container_group.get_child(first_free_container_index)
			var old_global_transform = card.get_global_transform()
			card.parent.remove_child(card)
			new_parent.add_child(card)
			card.parent = new_parent
			card.responsible_parent = self
			card.set_global_transform(old_global_transform)
			first_free_container_index += 1


func _process(delta) -> void:
	if GameState.card_in_hand or not _visible_mouse_box_rect.has_point(_viewport.get_mouse_position()):
		deck_target_offset = deck_offset_hidden
#		$CardsUp.play()
	elif _hidden_mouse_box_rect.has_point(_viewport.get_mouse_position()):
		deck_target_offset = deck_offset_visible
	_containers.position.y = lerp(_containers.position.y, deck_target_offset, (1 / deck_lerp_weight) * delta)
