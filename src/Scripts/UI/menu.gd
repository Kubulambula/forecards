extends Control


var tween_direction_out = true # out = menu away
var running = false
onready var tween = $Tween
var done = true
var can_esc = true
var quit = false 

func _ready():
	Music.mute_sfx = false
	get_tree().paused = true
	
	if Globals.load_err == ERR_PRINTER_ON_FIRE or (OS.has_feature("web") and not Restarter.intro_shown):
		yield(get_tree().create_timer(.1), "timeout")
		_play_cutscene()
		_on_Help_pressed()


func _on_Play_pressed():
	Restarter.restarting = false
	# warning-ignore:return_value_discarded
	Restarter.tw.stop_all()
	Music.filter_off(1.5)
	done = false
	for card in GameState.active_deck._all_cards:
		card.visible = true
	get_tree().paused = false
	tween_direction_out = true
	tween.interpolate_property(GameState._active_scene_camera, "zoom", 
			Vector2(3,3), Vector2(2.3, 2.3), 1.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(self, "modulate", 
			Color(1,1,1,1), Color(1,1,1,0), 1, Tween.TRANS_SINE, Tween.EASE_OUT)
		
	for mrak in get_tree().get_nodes_in_group("mrak"):
		tween.interpolate_property(mrak, "modulate", 
			Color(1,1,1,1), Color(1,1,1,0), 1, Tween.TRANS_SINE, Tween.EASE_OUT)
	
	tween.start()
	
	if not running:
		running = true
		yield(get_tree().create_timer(1.5), "timeout") # pockej, nez posles prvni kartu
		GameDirector.set_difficulty(0)
#		GameDirector.set_difficulty(1)
#		GameDirector.set_difficulty(2)
#		GameDirector.set_difficulty(3)
#		GameDirector.set_difficulty(4)
#		GameDirector.set_difficulty(5)
#		GameDirector.set_difficulty(6)
#		GameDirector.set_difficulty(7)
		GameState.spawner.start()
		
	if GameState.card_in_hand:
		GameState.card_in_hand.transfer_card_to_canvas()
		GameState.card_in_hand = null


func _on_Restart_pressed():
	Restarter.restart()


func _on_Options_pressed():
	$Options.visible = true


func _apply_volume(volume):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), volume)


func _on_Quit_pressed():
	if not OS.has_feature("web"):
		Restarter.restarting = true
		tween.interpolate_method(self, "_apply_volume", AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Master")), -80, 0.4, Tween.TRANS_SINE, Tween.EASE_IN)
		tween.start()
		quit = true


func _on_Tween_tween_all_completed():
	done = true
	if not modulate.a:
		visible = false
		$ButtonContainer/Play/Label.text = "Continue"
	if quit:
		Globals._notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)


func _input(event):
	if event is InputEventKey and event.pressed and event.scancode == KEY_ESCAPE and done and can_esc and not event.is_echo():
		if not modulate.a:
			Music.filter_on(2)
			visible = true
			done = false
			for card in GameState.active_deck._all_cards:
				card.visible = false
			
			tween_direction_out = false
			tween.interpolate_property(GameState._active_scene_camera, "zoom", 
			Vector2(2.3,2.3), Vector2(3,3), 1.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
			tween.interpolate_property(self, "modulate", 
					Color(1,1,1,0), Color(1,1,1,1), 1, Tween.TRANS_SINE, Tween.EASE_OUT, 0.5)
			
			for mrak in get_tree().get_nodes_in_group("mrak"):
				tween.interpolate_property(mrak, "modulate", 
					Color(1,1,1,0), Color(1,1,1,1), 1, Tween.TRANS_SINE, Tween.EASE_OUT)
			
			$Tween.start()
			get_tree().paused = true
#			var m_time = $BGMusic.get_playback_position()
#			$BGMusic.stop()
#			$MenuMusic.play(m_time)
		elif not $Options.visible:
			_on_Play_pressed()
	
	
	if event is InputEventKey and event.pressed and event.scancode == KEY_I and not event.is_echo():
		if $CanvasLayer/WindowDialog.visible:
			$CanvasLayer/WindowDialog.hide()
		else:
			$CanvasLayer/WindowDialog.popup(Rect2(560,240,560,240))


func dead_menu():
	Music.mute_sfx = true
	if can_esc:
		$ButtonContainer/DeathScreen/TimeSurvived.text = "Time survived:  %02d:%02d" % Time.get_time_survived()
	Music.filter_on(2)
	can_esc = false
	$ButtonContainer/Play.visible = false
	$ButtonContainer/Restart.visible = true
	$ButtonContainer/Logo.visible = false
	$ButtonContainer/DeathScreen.visible = true
	visible = true
	done = false
	for card in GameState.active_deck._all_cards:
		card.visible = false
	
	tween_direction_out = false
	tween.interpolate_property(GameState._active_scene_camera, "zoom", 
	Vector2(2.3,2.3), Vector2(3,3), 1.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(self, "modulate", 
			Color(1,1,1,0), Color(1,1,1,1), 1, Tween.TRANS_SINE, Tween.EASE_OUT, 0.5)
	
	for mrak in get_tree().get_nodes_in_group("mrak"):
		tween.interpolate_property(mrak, "modulate", 
			Color(1,1,1,0), Color(1,1,1,1), 1, Tween.TRANS_SINE, Tween.EASE_OUT)
	
	$Tween.start()
	GameState.spawner.max_weather_count = 1000
	GameState.spawner.set_pause_mode(PAUSE_MODE_PROCESS)
	for weather in get_tree().get_nodes_in_group("weather"):
		weather.get_parent().set_pause_mode(PAUSE_MODE_PROCESS)
	get_tree().paused = true 
	# TODO : Show time or some shit idk


func _on_Help_pressed():
	$Manual.visible = true
	$Manual.pressed = 1
	can_esc = false


func _on_Cutscene_pressed():
	$ButtonContainer/Cutscene.release_focus()
	Restarter.blink()
	yield(get_tree().create_timer(0.15), "timeout")
	_play_cutscene()


func _play_cutscene():
	$MouseStop.visible = true
	can_esc = false
	GameState._active_scene_camera.current = false
	$Cutscene.camera.current = true
	$Cutscene.can = true
	$Cutscene/Blocks/Cutscene0Block1/AnimationPlayer.seek(0, true)
