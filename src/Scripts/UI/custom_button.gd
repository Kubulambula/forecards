tool
extends Sprite

signal pressed
signal released
signal hover(value)


export(String) var text = "Sample Text" setget set_text
export(Vector2) var hover_scale = Vector2(1.1, 1.1)

var _scale = Vector2(1,1)
export(bool) var disabled = false

func _ready():
	$MarginContainer/Label.text = text
	_scale = scale
	hover_scale = scale * hover_scale


func set_text(value: String) -> void:
	text = value
	$MarginContainer/Label.text = value


func _on_MarginContainer_mouse_entered():
	scale = hover_scale
	emit_signal("hover", true)


func _on_MarginContainer_mouse_exited():
	scale = _scale
	emit_signal("hover", false)


func _on_MarginContainer_gui_input(event):
	if not disabled:
		if event is InputEventMouseButton and event.button_index == 1 and event.pressed and not event.is_echo():
			scale = _scale
			emit_signal("pressed")
			
		elif event is InputEventMouseButton and event.button_index == 1 and not event.pressed and not event.is_echo():
			scale = hover_scale
			emit_signal("released")
