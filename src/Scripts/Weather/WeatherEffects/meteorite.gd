extends Node2D

enum TYPE {INVALID, ACID, BLIZZARD, FIRE, RAIN, METEORITE, LIGHTNING, LOCUSTS, TORNADO}
const type = TYPE.METEORITE
const deadly = true

var spawn: Vector2
var target: Node2D

var _t: float = 0

onready var shadow: Sprite = $Shadow


func setup(_Spawn: Vector2, Target: Node2D, _Speed: float) -> void:
	spawn = Target.global_position + Vector2(4000, -4000)
	global_position = Target.global_position
	target = Target
	$Tween.interpolate_property(shadow, "scale", Vector2(0,0), Vector2(1.2,0.8), 5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.interpolate_property($GFX, "scale", Vector2(0,0), Vector2(1,1), 5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()


func _process(delta) -> void:
	_t += delta/5.1 # Just offset time so that it land on the 5th second (impact sound)
	_t = min(_t, 1)
	if _t == 1:
		$Area2D/CollisionShape2D.disabled = false
		delete()
		set_process(false)
	global_position = spawn.linear_interpolate(target.global_position, _t)
	shadow.global_position = target.global_position


func delete() -> void:
	$Particles2D.emitting = true
	$Particles2D2.emitting = true
	$GFX.visible = false
	$Shadow.visible = false
	GameDirector.increment_passed_effects()
	GameState.spawner.weather_count -= 1
	yield(get_tree().create_timer(3), "timeout")
	queue_free()
