# Copyright (c) 2021 Jakub Janšta
# See License.md

extends Node

#const daytime = 5

enum WINDOW_MODE {WINDOWED, FULLSCREEN, BORDERLESS}

var config: _Config = _Config.new() setget _set_discard

var load_err: int = 0


func _init() -> void:
	var success: bool = ProjectSettings.load_resource_pack("user://patch.pck")
	if success:
		print("patch.pck loaded successfuly")
	else:
		print("patch.pck loaded unsuccessfuly (non existent or corrupted)")
	add_child(preload("res://Scenes/UI/screen_border.tscn").instance())
	load_err = config.load_data()


# Just in case youn don't want somebody to modify your stuff
func _set_discard(_value) -> void:
	push_error("Do not modify this value")


func _notification(what: int) -> void:
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		print("Saving config err: ", config.save_data())
		get_tree().quit()



# Wrapper for ConfigFile so it can be used safely from outside
class _Config:
	const config_file_path: String = "user://config.cfg"
	const default_config_file_path: String = "res://Misc/default_config.cfg"
	var _cfgf: ConfigFile
	
	
	func _init() -> void:
		_cfgf = ConfigFile.new()
	
	
	func load_data() -> int:
		var err: int = _cfgf.load(config_file_path)
		
		if err == OK:
			print("Confing found. Loading...")
			return OK
		elif err == ERR_FILE_NOT_FOUND:
			err = _cfgf.load(default_config_file_path)
			if err == OK:
				print("Config not found. Using default...")
				return ERR_PRINTER_ON_FIRE
			else:
				printerr("Error while reading default config data. Err: " + str(err))
				return err
			
		else:
			printerr("Error while reading config data. Err: " + str(err))
			return err
	
	
	func save_data() -> int:
		var err: int = _cfgf.save(config_file_path)
		if err:
			printerr("Error while saving config data. Err: " + str(err))
		else:
			print("Saving config data...")
		return err
	
	
	func set_value(section: String, key: String, value) -> void:
		_cfgf.set_value(section, key, value)
	
	
	func get_value(section: String, key: String, default):
		return _cfgf.get_value(section, key, default)
