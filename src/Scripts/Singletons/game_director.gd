extends Node

enum CARD {INVALID, ACID, BLIZZARD, FIRE, RAIN, METEORITE, LIGHTNING, LOCUSTS, TORNADO}
const PR_CARD = preload("res://Scenes/Cards/card.tscn")


var passed_effects: int = 0
var difficulty: int = 0 setget set_difficulty

var running = true

var progress: Array = [
#	{"weather":[{"meteorite":CARD.METEORITE}], "spawner":{"tick":10, "max": 3, "speed": 1}},
#	{"weather":[{"meteorite":CARD.METEORITE}], "spawner":{"tick":10, "max": 3, "speed": 1}},
#	{"weather":[{"meteorite":CARD.METEORITE}], "spawner":{"tick":10, "max": 3, "speed": 1}},
#	{"weather":[{"blizzard":CARD.BLIZZARD}], "spawner":{"tick":7, "max": 13, "speed": 1.1}},
#	{"weather":[{"blizzard":CARD.BLIZZARD}], "spawner":{"tick":7, "max": 13, "speed": 1.1}},
#	{"weather":[{"blizzard":CARD.BLIZZARD}], "spawner":{"tick":7, "max": 13, "speed": 1.1}},
	
	{"weather":[{"rain":CARD.RAIN}], "spawner":{"tick":9, "max": 3, "speed": 0.9}},
	{"weather":[{"lightning":CARD.LIGHTNING}], "spawner":{"tick":8, "max": 7, "speed": 1}},
	{"weather":[{"blizzard":CARD.BLIZZARD}], "spawner":{"tick":7, "max": 13, "speed": 1}},
	{"weather":[{"locusts":CARD.LOCUSTS}], "spawner":{"tick":6, "max": 16, "speed": 1}},
	{"weather":[{"acid":CARD.ACID}], "spawner":{"tick":5, "max": 20, "speed": 1.05}},
	{"weather":[{"fire":CARD.FIRE}], "spawner":{"tick":4, "max": 25, "speed": 1.1}},
	{"weather":[{"tornado":CARD.TORNADO}], "spawner":{"tick":3.5, "max": 30, "speed": 1.13}},
	{"weather":[{"meteorite":CARD.METEORITE}], "spawner":{"tick":3, "max": 30, "speed": 1.15}},
]


# Trggered by weather deletion. Tracks how many passed to adjust difficulty
func increment_passed_effects() -> void:
	passed_effects += 1
	if passed_effects == 1:
		set_difficulty(1)
	elif passed_effects == 5:
		set_difficulty(2)
	elif passed_effects == 10:
		set_difficulty(3)
	elif passed_effects == 15:
		set_difficulty(4)
	elif passed_effects == 22:
		set_difficulty(5)
	elif passed_effects == 30:
		set_difficulty(6)
	elif passed_effects == 40:
		set_difficulty(7)


func set_difficulty(value: int) -> void:
	if running:
		difficulty = value
		var difficulty_settings = progress[value]
		# Add new cards and effects
		for effect in difficulty_settings["weather"]:
			var key = effect.keys()[0]
			var card = PR_CARD.instance()
			card.set_type(effect[key])
			yield(get_tree().create_timer(.1), "timeout")
			GameState.active_deck.call("add_card", card)
			GameState.spawner.allowed_weather_effects.append(key)
		# Update modifiers
		GameState.spawner.set_tick_rate(difficulty_settings["spawner"]["tick"])
		GameState.spawner.max_weather_count = difficulty_settings["spawner"]["max"]
		GameState.spawner.weather_speed = difficulty_settings["spawner"]["speed"]
#		if difficulty != 0:
#			GameState.spawner.tick()


func check_cities() -> void:
	if not GameState.active_cities.size() and running: # All cities have been destroyed
		running = false
		GameState.active_scene.get_node("MenuContainer/Menu").dead_menu()
		printerr("All cities destroyed")
