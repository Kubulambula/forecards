extends Node2D


#signal block_animation_ended(block_index)
#signal block_started(block_index)
#signal cutscene_ended


var blocks = []
var block_index = 0

var can = false

onready var camera = $Camera2D


func _ready():
	blocks = $Blocks.get_children()
	$Camera2D.global_position = blocks[0].camera_target_position
	next_block()


func next_block():
	if block_index < blocks.size():
#		emit_signal("block_started", block_index)
#		print("block_started", block_index)
		$Tween.interpolate_property(
				$Camera2D,
				"global_position", 
				$Camera2D.global_position,
				blocks[block_index].camera_target_position,
				blocks[block_index].duration,
				blocks[block_index].trans_type,
				blocks[block_index].ease_type,
				blocks[block_index].delay)
		$Tween.start()
		
#		blocks[block_index].connect("block_animation_ended", self, "_on_block_animation_ended", [], CONNECT_ONESHOT)
		blocks[block_index].start()
		block_index += 1
	else:
		Restarter.blink()
		yield(get_tree().create_timer(0.15), "timeout")
		GameState._active_scene_camera.current = true
		$Camera2D.current = false
		can = false
		$Tween.stop_all()
		$Camera2D.global_position = blocks[0].camera_target_position
		block_index = 1
		yield(get_tree().create_timer(0.5), "timeout")
		GameState.active_scene.get_node("MenuContainer/Menu").can_esc = true
		GameState.active_scene.get_node("MenuContainer/Menu/MouseStop").visible = false
		
#		emit_signal("cutscene_ended")


# TODO : This is TEMPORARY
func _input(event: InputEvent) -> void:
	if (
		(event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed) 
		or (event is InputEventKey and event.pressed and not event.is_echo()
		and (event.scancode == KEY_ENTER or event.scancode == KEY_SPACE or event.scancode == KEY_ESCAPE))
	):
		if not $Tween.is_active() and can:
			next_block()

#func _on_block_animation_ended():
#	emit_signal("block_animation_ended", block_index - 1) # The index was incremented
#	print("block_animation_ended", block_index)

