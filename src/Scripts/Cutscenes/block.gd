extends Node2D

signal block_animation_ended

var camera_target_position: Vector2
# Tyhle proměnný se použijou, na transition NA TUTO SCÉNU
export(bool) var use_camera_global_position = false
export(Vector2) var camera_global_position = Vector2(0,0)
export(int, 0, 10) var trans_type = 4
export(int, 0, 3) var ease_type = 2
export(float, 0, 1000) var duration = 1.5
export(float, 0, 1000) var delay = 0


func _ready():
	camera_target_position = camera_global_position if use_camera_global_position else global_position + Vector2(960, 540)


func start():
	$AnimationPlayer.play("Block_anim")


func _on_AnimationPlayer_animation_finished(_anim_name):
	emit_signal("block_animation_ended")
	# TODO : Play again
