tool
extends Node2D


export(float) var spawn_edge_radius = 2500 setget set_spawn_edge_radius
var sec_tick_rate = 1 setget set_tick_rate

const _WEATHER_EFFECTS: Dictionary = {
	"acid": preload("res://Scenes/Weather/WeatherEffects/acid.tscn"),
	"blizzard": preload("res://Scenes/Weather/WeatherEffects/blizzard.tscn"),
	"fire": preload("res://Scenes/Weather/WeatherEffects/fire.tscn"),
	"rain": preload("res://Scenes/Weather/WeatherEffects/rain.tscn"),
	"locusts": preload("res://Scenes/Weather/WeatherEffects/locusts.tscn"),
	"tornado": preload("res://Scenes/Weather/WeatherEffects/tornado.tscn"),
	"lightning": preload("res://Scenes/Weather/WeatherEffects/lightning.tscn"),
	"meteorite": preload("res://Scenes/Weather/WeatherEffects/meteorite.tscn"),
}

var allowed_weather_effects: Array = []

var weather_count: int = 0
var max_weather_count: int = 0

var base_weather_speed: float = 150
var weather_speed: float = 1

var _last_city_index: int = 0
var _last_weather_index: int = -1


func set_spawn_edge_radius(value: float) -> void:
	spawn_edge_radius = value
	if get_node_or_null("Area2D/CollisionShape2D"):
		$Area2D/CollisionShape2D.shape.radius = value


func set_tick_rate(value: float = 1) -> void:
	sec_tick_rate = value
	if not Engine.editor_hint and get_node_or_null("TickTimer"):
		$TickTimer.wait_time = value


func _ready() -> void:
	if not Engine.editor_hint:
		GameState.spawner = self
		randomize()



func start() -> void:
		tick()
		# warning-ignore:return_value_discarded
		$TickTimer.connect("timeout", self, "tick")
		$TickTimer.start()


func tick() -> void:
	if weather_count < max_weather_count:
		weather_count += 1
		var city = select_random_city()
		var weather = select_random_weather()
		var spawn = select_weather_spawn_point()
		add_child(weather)
		weather.setup(spawn, city, base_weather_speed * weather_speed)


func select_random_city() -> Node2D:
	if GameState.active_cities.size() > 1: # Two or more cities
		var index = randi() % GameState.active_cities.size()
		while index == _last_city_index:
			index = randi() % GameState.active_cities.size()
		_last_city_index = index
		return GameState.active_cities[index]
	elif GameState.active_cities.size() == 1: # Just one city
		if randi() % 4: # 3/4 attack the last active city. Otherwise attack random city which is already dead
			return GameState.active_cities[0]
	
	return GameState.all_cities[randi() % GameState.all_cities.size()]
#	return null # No more cites


# Selects random point in global cords on the circle of spawn_edge_radius
func select_weather_spawn_point() -> Vector2:
	return Vector2(spawn_edge_radius, 0).rotated(randf() * 2 * PI) + Vector2(960, 540)


func select_random_weather() -> Node2D:
	var index: int = randi() % allowed_weather_effects.size()
	if allowed_weather_effects.size() > 1:
		while index == _last_weather_index:
			index = randi() % allowed_weather_effects.size()
	_last_weather_index = index
	return _WEATHER_EFFECTS[allowed_weather_effects[index]].instance()


func _on_Area2D_area_exited(area):
	if area.is_in_group("weather") and not Restarter.restarting:
		area.get_parent().delete()
		GameDirector.increment_passed_effects()
		weather_count -= 1
