extends Control



func _ready():
	if OS.has_feature("web"):
		$"Panel/MarginContainer/HBoxContainer/VBoxContainer".visible = false
	# Apply saved settings
	$Panel/MarginContainer/HBoxContainer/VBoxContainer/FCButton.pressed = Globals.config.get_value("graphics", "fullscreen", true) as bool
	$Panel/MarginContainer/HBoxContainer/VBoxContainer2/SliderMusic.value = Globals.config.get_value("audio", "music", 100) as float
	$Panel/MarginContainer/HBoxContainer/VBoxContainer3/SliderSFX.value = Globals.config.get_value("audio", "sfx", 100) as float
	$Panel/MarginContainer/HBoxContainer/VBoxContainer4/SliderMaster.value = Globals.config.get_value("audio", "master", 100) as float


func _on_FCButton_toggled(button_pressed):
	if not OS.has_feature("web"):
		Globals.config.set_value("graphics", "fullscreen", button_pressed)
		OS.window_fullscreen = button_pressed


func _on_SliderMaster_value_changed(value):
	Globals.config.set_value("audio", "master", value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db(value/100))
#			Util.remap_value_between_ranges(value, Vector2(0,100), Vector2(-80,0)))


func _on_SliderMusic_value_changed(value):
	Globals.config.set_value("audio", "music", value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear2db(value/100))
#			Util.remap_value_between_ranges(value, Vector2(0,100), Vector2(-80,-5)))


func _on_SliderSFX_value_changed(value):
	Globals.config.set_value("audio", "sfx", value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), linear2db(value/100))
#			Util.remap_value_between_ranges(value, Vector2(0,100), Vector2(-80,0)))


func _on_Hide_pressed():
	visible = false


func _input(event):
	if event is InputEventKey and event.pressed and event.scancode == KEY_ESCAPE and visible:
		yield(get_tree().create_timer(0.1), "timeout")
		visible = false
