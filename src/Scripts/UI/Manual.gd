extends Control

var pressed = 1


func _on_Button_pressed():
	pressed += 1
#	if pressed == 0:
#		$All.visible = false
	if pressed == 1:
		$All/Types.visible = true
	if pressed == 2:
		$All/Good.visible = true
	if pressed == 3:
		$All/Bad.visible = true
	if pressed == 4:
		$All/CardsNonDeadly.visible = true
	if pressed == 5:
		$All/CardsDeadly.visible = true
		$All/Button.visible = false
		$All/Cross.visible = true


func _on_Cross_pressed():
	visible = false
	$All/Types.visible = true
	$All/Good.visible = false
	$All/Bad.visible = false
	$All/CardsNonDeadly.visible = false
	$All/CardsDeadly.visible = false
	$All/Button.visible = true
	$All/Cross.visible = false
	pressed = 1
	get_parent().can_esc = true
